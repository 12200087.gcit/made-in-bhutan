// middleware/totalCartItemsMiddleware.js
const Cart = require('../models/userCartModel');

async function totalCartItemsMiddleware(req, res, next) {
  try {
    const cartItems = await Cart.find({ userId: req.session.userId }).populate('productId');
    const totalCartItems = cartItems.length;

    res.locals.totalCartItems = totalCartItems;
    next();
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
}

module.exports = totalCartItemsMiddleware;
