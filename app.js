const express = require("express");
const expressejslayouts = require('express-ejs-layouts');
const session = require('express-session');
const flash = require('express-flash');
const path = require('path');
const authRoutes = require('./routes/authRoutes');
const totalCartItemsMiddleware = require('./middleware/TotalCardItems'); 

const app = express();
app.use(expressejslayouts)
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.urlencoded({ extended: false }));
app.use(express.static('public'));
app.use("/assets", express.static(__dirname + "/assets"));


app.use(
    session({
      secret: 'your-secret-key',
      resave: false,
      saveUninitialized: false,
    })
  );
  app.use(flash());
  
  app.use((req, res, next) => {
    res.locals.user = req.session.userId;
    next();
  });
  app.use(totalCartItemsMiddleware)
  app.use(authRoutes);

// Routes
const homeRoutes = require("./routes/userRoutes/home");
const aboutRoutes = require("./routes/userRoutes/about");
const contactRoutes = require("./routes/userRoutes/contact");
const menRoutes = require("./routes/userRoutes/men");
const womenRoutes = require("./routes/userRoutes/women");
const shopRoutes = require("./routes/userRoutes/shop");
const cartRoutes = require("./routes/userRoutes/cart");
const profileRoutes = require("./routes/userRoutes/profile");
const searchRoutes = require("./routes/userRoutes/search");
const forgotPassword = require("./routes/userRoutes/forgotPassword");
app.use(express.json());
app.use(homeRoutes);
app.use(aboutRoutes);
app.use(contactRoutes);
app.use(menRoutes);
app.use(womenRoutes);
app.use(shopRoutes);
app.use(cartRoutes);
app.use(profileRoutes);
app.use(searchRoutes);
app.use(forgotPassword);

// Create a route for the "Not Found" page
app.get('*', (req, res) => {
  
  res.status(404).render('404',{
    title:'Not Found',
    layout: './userLayouts/404layouts'
  });
});


// Include product routes
const productRoutes = require('./routes/productRoutes');
app.use(productRoutes);



// Database configuration
require("dotenv").config();
require("./config/dbConnection.js")();

// server side
const port = 8000;
app.listen(port, console.log(`Server is running at http://localhost:${port}`));
