// const mongoose = require("mongoose");
// let db;
// const connectDB = async() => {
// 	try
// 	{
// 		db = process.env.MONGO_URI;
// 		await mongoose.connect(db);
// 		console.log("Mongo DB connected...");
// 	}
// 	catch(err)
// 	{
// 		console.log(err);
// 		process.exit(1);
// 	}
// }

// module.exports = connectDB;


const mongoose = require('mongoose');

const connectDB = async () => {
  try {
    const uri = "mongodb+srv://12200084gcit:bhutan_fashion123@cluster0.ltn6oqs.mongodb.netbhutan-fashion?retryWrites=true&w=majority";
    const options = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };

    await mongoose.connect(uri, options);

    console.log('Connected to MongoDB');
  } catch (error) {
    console.error('MongoDB connection error:', error.message);
    process.exit(1); // Exit process with failure
  }
};

module.exports = connectDB;
