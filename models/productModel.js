const mongoose = require('mongoose');
const { Schema } = mongoose;

const productSchema = new Schema({
  productname: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  productcategory: {
    type: String,
    required: true,
  },
  productimage: {
    type: String,
    required: true,
  },
  size: {
    type: [String],
    required: true,
  },
},
{
  timestamps: true
});

const Product = mongoose.model('Product', productSchema);

module.exports = Product;
