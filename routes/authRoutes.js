const express = require('express');
const authController = require('../controllers/authController');
const router = express.Router();
const bcrypt = require('bcrypt');
const User = require('../models/userModel');

router.get('/login', authController.getLogin);
router.post('/login', authController.postLogin);
router.get('/register', authController.getRegister);
router.post('/register', authController.postRegister);
router.get('/logout', authController.logout);

// Define a route for updating the password
router.post('/update-password', async (req, res) => {
    const { oldPassword, newPassword, confirmPassword } = req.body;
  
    try {
      // Find the user by their unique identifier (e.g., user ID from a session or JWT)
      const userId = req.session.userId;
      const user = await User.findById(userId); 
  
      if (!user) {
        req.flash('error', 'User not found.');
        return res.redirect('/profile');
      }
  
      // Check if the old password matches the hashed password in the database
      const isPasswordValid = await bcrypt.compare(oldPassword, user.password);
  
      if (!isPasswordValid) {
        req.flash('error', 'Old password is incorrect.');
        return res.redirect('/profile');
      }
  
      // Validate the new password and confirmation
      if (newPassword !== confirmPassword) {
        req.flash('error', 'New password and confirmation do not match.');
        return res.redirect('/profile');
      }
  
      // Hash the new password
      const hashedNewPassword = await bcrypt.hash(newPassword, 10);
  
      // Update the user's password with the new hashed password
      user.password = hashedNewPassword;
  
      // Save the updated user information
      await user.save();
  
      req.flash('success', 'Password updated successfully.');
      return res.redirect('/profile'); // Redirect to the login page or any other appropriate destination.
    } catch (error) {
      // Handle any server-side errors here
      console.error(error);
      req.flash('error', 'An error occurred while updating the password.');
      return res.redirect('/profile');
    }
  });

module.exports = router;
