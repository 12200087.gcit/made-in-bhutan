const express = require('express');
const router = express.Router();
const authController = require('../../controllers/authController');

// Display forgot password form
router.get('/forgotPassword', authController.forgotPassword);

// Handle forgot password form submission
router.post('/forgotPassword', authController.postForgotPassword);


// Display reset password form
router.get('/resetPassword/:token', authController.getResetPassword);

// Handle reset password form submission
router.post('/resetPassword/:token', authController.postResetPassword);

module.exports = router;