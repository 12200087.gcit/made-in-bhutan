const express = require("express");
const router = express.Router();
const User = require('../../models/userModel');

// Middleware to check if the user is authenticated
function isAuthenticated(req, res, next) {
	if (req.session.userId) {
	  // If the user is authenticated, proceed to the page.
	  return next();
	} else {
	  // If the user is not authenticated, redirect to the login page.
	  req.flash('error', 'You must be signed in to access this page.');
	  res.redirect('/login');
	}
  }


const multer = require('multer'); // Require multer

// Define storage and file upload configuration
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/'); // Define the directory where uploaded files will be stored
  },
  filename: function (req, file, cb) {
    // Generate a unique filename (you can customize this)
    cb(null, Date.now() + '-' + file.originalname);
  },
});

const upload = multer({ storage: storage });

// route to render profile page
router.get('/profile',isAuthenticated, async (req, res) => {
    try {
		const userId = req.session.userId;
        const user = await User.findById(userId); 

        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }
		const title = "Profile Setting";
        res.render('user/profile',
		 {title,layout:'./userLayouts/UserLayouts',
		 activeTab: 'profile', user , 
		 successMessage: req.flash('success'),
		 errorMessage: req.flash('error')
		});
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
});

router.post('/profile', isAuthenticated, upload.single('profileImage'), async (req, res) => {
	try {
	  const userId = req.session.userId;
	  const user = await User.findById(userId);
  
	  if (!user) {
		req.flash('error', 'User not found');
		return res.redirect('/profile');
	  }
  
	  // Check if the provided email already exists for another user
	  const existingUser = await User.findOne({ email: req.body.email });
	  if (existingUser && existingUser._id.toString() !== userId) {
		req.flash('error', 'Email already in use');
		return res.redirect('/profile');
	  }
  
	  // Update user profile based on form data
	  user.name = req.body.name;
	  user.email = req.body.email;
  
	  if (req.file) {
		// Handle file upload
		user.profileImage = req.file.filename;
	  }
  
	  // Save the updated user to the database
	  await user.save();
  
	  req.flash('success', 'Profile updated successfully');
	  res.redirect('/profile');
	} catch (error) {
	  console.error(error);
	  req.flash('error', 'Internal server error');
	  res.redirect('/profile');
	}
  });
  
  



module.exports = router;