const express = require("express");
const router = express.Router();

router.get("/about", (req,res) => {
	const title = "About";
	res.render("user/about",{title,layout:'./userLayouts/UserLayouts',activeTab: 'about'});
});


module.exports = router;