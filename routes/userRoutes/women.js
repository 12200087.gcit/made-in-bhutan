const express = require("express");
const router = express.Router();
const Product = require('../../models/productModel');

router.get("/women", async (req, res) => {
  try {
    const title = "Women";
    const womenProducts = await Product.find({ productcategory: 'Women' }).sort({ createdAt: -1 });

    res.render("user/Women", { title, layout: './userLayouts/UserLayouts', activeTab: 'women', womenProducts });
  } catch (error) {
    console.error("Error fetching women products:", error);
  }
});

module.exports = router;
