const express = require('express');
const router = express.Router();
const cartController = require('../../controllers/cartController');

// Middleware to check if the user is authenticated
function isAuthenticated(req, res, next) {
  if (req.session.userId) {
    // If the user is authenticated, proceed to the page.
    return next();
  } else {
    // If the user is not authenticated, redirect to the login page.
    req.flash('error', "Access to your cart's content requires login");
    res.redirect('/login');
  }
}

// Route to add a product to the cart
router.post('/mycart',isAuthenticated, cartController.addToCart);

// Route to display and update cart
router.get('/mycart',isAuthenticated, cartController.displayCart);

// Delete a product from the cart
router.post('/deleteCartItem/:itemId', cartController.deleteFromCart);


router.post('/mycart/updateQuantityInCart/:itemID', cartController.updateQuantityInCart);


module.exports = router;

