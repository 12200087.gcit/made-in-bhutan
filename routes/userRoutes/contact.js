const express = require('express');
const router = express.Router();
const contactController = require('../../controllers/contactFormController');


// Route to add a product to the cart
router.post('/contact',contactController.submitContactForm);

// Route to display and update cart
router.get('/contact',contactController.renderContactForm);


module.exports = router;

