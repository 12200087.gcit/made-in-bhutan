const express = require('express');
const router = express.Router();
const Product = require('../../models/productModel')

// Route to handle product search request
router.get('/search-products', (req, res) => {
    const { productcategory, searchTerm } = req.query;
    const title = 'Searh'
    // Query the database to search for products
    Product.find({
      $and: [
        { productcategory: productcategory !== 'all' ? productcategory : { $exists: true } },
        { productname: { $regex: searchTerm, $options: 'i' } }, // Case-insensitive search
      ],
    })
      .then((products) => {
        res.render('user/search', {title, layout: './userLayouts/UserLayouts', activeTab: 'searh', products,searchTerm });
      })
      .catch((error) => {
        console.error(error);
        res.status(500).send('Internal Server Error');
      });
  });
  
module.exports = router;



