const express = require("express");
const router = express.Router();
const Product = require('../../models/productModel'); 

router.get("/", async (req, res) => {
  try {
    const successMessage = req.flash('success')[0];
    const title = "Home";
    const womenProducts = await Product.find({ productcategory: 'Women' }).sort({ createdAt: -1 });
    const menProducts = await Product.find({ productcategory: 'Men' }).sort({ createdAt: -1 });

    res.render("user/home", { title, layout: './userLayouts/UserLayouts', womenProducts,menProducts,activeTab: 'home',successMessage});
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
