const express = require("express");
const router = express.Router();
const Product = require('../../models/productModel');

router.get("/shop", async (req, res) => {
  try {
    const title = "Shop";
    const allProducts = await Product.aggregate([{ $sample: { size: 40 } }]);
    res.render("user/shop", { title, layout: './userLayouts/UserLayouts', activeTab: 'shop', allProducts });
  } catch (error) {
    console.error("Error fetching products:", error);
  }
});

module.exports = router;
