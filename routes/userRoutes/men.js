const express = require("express");
const router = express.Router();
const Product = require('../../models/productModel');

router.get("/men", async (req, res) => {
  try {
    const title = "Men";
    const menProducts = await Product.find({ productcategory: 'Men' }).sort({ createdAt: -1 });

    res.render("user/men", { title, layout: './userLayouts/UserLayouts', activeTab: 'men', menProducts });
  } catch (error) {
    console.error("Error fetching men products:", error);
  }
});

module.exports = router;
