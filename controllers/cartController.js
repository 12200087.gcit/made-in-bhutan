const Cart = require('../models/userCartModel');


// Add product to cart
exports.addToCart = async (req, res) => {
  try {
    const { productId, size, quantity } = req.body;
    const userId = req.session.userId; // Get the user's ID from the session

    // Check if the product is already in the user's cart
    let cartItem = await Cart.findOne({ userId, productId, size });

    if (cartItem) {
      // If the product already exists in the cart, update the quantity
      cartItem.quantity = parseInt(cartItem.quantity) + parseInt(quantity);
    } else {
      // If the product doesn't exist in the cart, create a new cart item
      cartItem = new Cart({
        userId,
        productId,
        size,
        quantity,
      });
    }

    // Save the cart item to the database
    await cartItem.save();

    res.redirect('/mycart'); // Redirect the user to the cart page
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
};

// Display and update cart page
exports.displayCart = async (req, res) => {
  
  user_id = req.session.userId;
  try {
    const userId = req.session.userId; // Get the user's ID from the session
    const cartItems = await Cart.find({ userId }).populate('productId');
    const totalCartItems = cartItems.length;
    const title = "Cart";
    res.render('user/cart', {title,totalCartItems, layout: './userLayouts/UserLayouts', cartItems,userId,activeTab: 'cart' });
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
};


// Delete product from cart
exports.deleteFromCart = async (req, res) => {
  try {
    const { itemId } = req.params; // Assuming you pass the item's ID in the URL
    const userId = req.session.userId; // Get the user's ID from the session

    // Use deleteOne to remove the item from the cart
    const result = await Cart.deleteOne({ _id: itemId, userId });

    if (result.deletedCount === 0) {
      // Handle the case where the item is not found in the user's cart
      return res.status(404).send('Item not found in your cart.');
    }

    res.redirect('/mycart'); // Redirect the user back to the cart page
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
};



// Update the quantity of an item in the cart
exports.updateQuantityInCart = async (req, res) => {
  try {
      const { itemID } = req.params;
      const { newQuantity } = req.body;
      const userId = req.session.userId;

      // Ensure newQuantity is a valid number
      const parsedQuantity = parseFloat(newQuantity);
      if (isNaN(parsedQuantity)) {
          return res.status(400).send('Invalid quantity value');
      }

      // Find the cart item by its ID and the user's ID
      const cartItem = await Cart.findOne({ _id: itemID, userId });

      if (!cartItem) {
          return res.status(404).send('Cart item not found.');
      }

      // Update the quantity
      cartItem.quantity = parsedQuantity;

      // Save the updated cart item
      await cartItem.save();

      res.status(200).send('Quantity updated successfully');
  } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
  }
}

