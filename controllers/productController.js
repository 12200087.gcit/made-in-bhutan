const express = require('express');
const router = express.Router();
const Product = require('../models/productModel');

// Controller function for rendering the product form
router.get('/add-product', (req, res) => {
  const title = 'Add Product';
  res.render('add-product', {
    title,
    layout: './userLayouts/auth'
  });
});

// Controller function for handling product form submission
router.post('/add-product', async (req, res) => {
  const { productname, price, productcategory, productimage, size } = req.body;

  try {
    const product = new Product({ productname, price, productcategory, productimage, size });
    await product.save();
    res.redirect('/');
  } catch (error) {
    res.status(500).send('Error adding the product');
  }
});

module.exports = router;
