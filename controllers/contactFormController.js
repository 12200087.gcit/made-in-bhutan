// Render the Contact Us form
exports.renderContactForm = (req, res) => {
    const title = 'Contact Us';
    res.render('user/contact', {
      title,
      layout: './userLayouts/UserLayouts',
      successMessage: req.flash('success'),
      activeTab: 'contact'
    });
  };

const Contact = require('../models/contactModel');

  // Handle the "Contact Us" form submission
  exports.submitContactForm = async (req, res) => {
    const { email, subject, message } = req.body;
  
    try {
      const contact = new Contact({ email, subject, message });
      await contact.save();
      req.flash('success', 'Contact form successfully submitted!');
      res.redirect('/contact'); // Redirect back to the contact form
    } catch (error) {
      res.status(500).send('Error submitting the contact form');
    }
  };
  