const bcrypt = require('bcrypt');
const User = require('../models/userModel');
const util = require('util');
const crypto = require('crypto');
const nodemailer = require('nodemailer');

// Middleware to check if the user is authenticated
function isAuthenticated(req, res, next) {
  if (req.session.userId) {
    // If the user is already authenticated, redirect them to a different page, e.g., the homepage.
    return res.redirect('/');
  }
  next();
}

exports.getLogin = [isAuthenticated, (req, res) => {
  const title = 'Login';
  res.render('user/login', {
    title,
    layout: './userLayouts/auth',
    successMessage: req.flash('success'),
    errorMessage: req.flash('error')
  });
}];


exports.postLogin = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });

  if (!user) {
    req.flash('error', 'This email is not registered!');
    return res.redirect('/login');
  }

  const passwordMatch = await bcrypt.compare(password, user.password);
  if (!passwordMatch) {
    req.flash('error', 'Incorrect password!');
    return res.redirect('/login');
  }

  req.session.userId = user._id;
  req.flash('success', 'Login successful!');
  res.redirect('/');
};

exports.getRegister = [isAuthenticated, (req, res) => {
  const title = 'Register';
  res.render('user/register', {
    title,
    layout: './userLayouts/auth',
    successMessage: req.flash('success'),
    errorMessage: req.flash('error')
  });
}];

exports.postRegister = async (req, res) => {
  const { email, password, name } = req.body;

  const existingUser = await User.findOne({ email });

  if (existingUser) {
    req.flash('error', 'This email already exists!');
    return res.redirect('/register');
  }

  const hashedPassword = await bcrypt.hash(password, 10);

  const user = new User({
    email,
    password: hashedPassword,
    name,
  });

  await user.save();
  req.flash('success', 'Registration successful. Please log in.');
  res.redirect('/login');
};

exports.logout = (req, res) => {
  req.flash('success', 'You have been successfully logged out.');
  req.session.destroy(() => {
    res.redirect('/');
  });
};

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: '12200084.gcit@rub.edu.bt',
    pass: 'sonamjigs',
  },
});

const randomBytesAsync = util.promisify(crypto.randomBytes);

const sendResetPasswordEmail = (email, token) => {
  const mailOptions = {
    to: email,
    from: 'fashionbhutan@gmail.com',
    subject: 'Password Reset (Fashion Bhutan)',
    html: `
      <p>You requested a password reset</p>
      <p>Click this <a href="http://localhost:8001/resetPassword/${token}">link</a> to set a new password.</p>
    `,
  };

  return transporter.sendMail(mailOptions);
};

exports.forgotPassword = [isAuthenticated, (req, res) => {
  const title = 'Forgot Password';
  res.render('user/forgotPassword', {
    title,
    layout: './userLayouts/auth',
    successMessage: req.flash('success'),
    errorMessage: req.flash('error'),
  });
}];

exports.postForgotPassword = async (req, res) => {
  try {
    const { email } = req.body;
    const token = (await randomBytesAsync(32)).toString('hex');

    const user = await User.findOne({ email: email });

    if (!user) {
      req.flash('error', 'Email not found!');
      return res.redirect('/forgotPassword');
    }

    user.resetToken = token;
    user.resetTokenExpiration = Date.now() + 3600000; // 1 hour

    await user.save();
    await sendResetPasswordEmail(email, token);

    req.flash('success', 'We have sent a link in your mail to reset the password. Please check your mail inbox');
    res.redirect('/forgotPassword');
  } catch (err) {
    console.error(err);
    req.flash('error', 'Something went wrong!');
    res.redirect('/forgotPassword');
  }
};

exports.getResetPassword = async (req, res) => {
  try {
    const token = req.params.token;
    const user = await User.findOne({
      resetToken: token,
      resetTokenExpiration: { $gt: Date.now() },
    });

    if (!user) {
      req.flash('error', 'Invalid or expired token');
      return res.redirect('/forgotPassword');
    }

    const title = 'Reset Password';
    res.render('user/reset-password', {
      title,
      layout: './userLayouts/auth',
      token,
      successMessage: req.flash('success'),
      errorMessage: req.flash('error'),
    });
  } catch (err) {
    console.error(err);
    req.flash('error', 'Something went wrong');
    res.redirect('/forgotPassword');
  }
};

// Handle reset password form submission
exports.postResetPassword = async (req, res) => {
  try {
    const { password, confirmPassword, token } = req.body;
    const user = await User.findOne({
      resetToken: token,
      resetTokenExpiration: { $gt: Date.now() },
    });

    if (!user || password !== confirmPassword) {
      req.flash('error', 'Invalid password or password mismatch');
      return res.redirect(`/resetPassword/${token}`); // Redirect back to the reset password page with an error message
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    user.password = hashedPassword ;
    user.resetToken = null;
    user.resetTokenExpiration = undefined;

    await user.save();
    req.flash('success', 'Password reset successfully');
    res.redirect('/login'); // Redirect to the login page
  } catch (err) {
    console.error(err);
    req.flash('error', 'Something went wrong');
    res.redirect(`/resetPassword/${token}`); // Redirect back to the reset password page with an error message
  }
};


